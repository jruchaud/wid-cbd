import { precacheAndRoute } from "workbox-precaching/precacheAndRoute.mjs";
import { createHandlerBoundToURL } from "workbox-precaching";
import { NavigationRoute, registerRoute } from "workbox-routing";

// Precache files with Workbox
precacheAndRoute(self.__WB_MANIFEST || []);

// This is a single page app. We need to register a default route (specific response) for all navigation requests.
// (see https://developers.google.com/web/tools/workbox/modules/workbox-routing#how_to_register_a_navigation_route)
const handler = createHandlerBoundToURL("/index.html");
const navigationRoute = new NavigationRoute(handler);
registerRoute(navigationRoute);

self.onnotificationclick = function (event) {
    event.waitUntil(
        self.clients
            .matchAll({
                type: "window",
            })
            .then(async (clientList) => {
                let client = clientList.find((c) => c.url.includes("/timeline"));
                if (!client) {
                    client = await self.clients.openWindow("/timeline");
                }
                client.postMessage({ action: event.action, date: event.notification.data });
                client.focus();
            }),
    );
};

self.addEventListener("message", (event) => {
    if (event.data && event.data.type === "SKIP_WAITING") {
        self.skipWaiting();
    }
});
