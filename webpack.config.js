const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WorkboxPlugin = require("workbox-webpack-plugin");
const packageJson = require("./package.json");
const simpleGit = require("simple-git");
const git = simpleGit();

module.exports = async (env, argv) => {
    const gitRev = await git.revparse(["HEAD"]);
    const now = new Date();

    return {
        entry: "./src/js/index.js",
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "wid.js",
        },
        resolve: {
            // Add `.ts` as a resolvable extension.
            extensions: [".vue", ".js"],
        },
        devtool: "source-map",
        devServer: {
            historyApiFallback: true,
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    loader: "string-replace-loader",
                    options: {
                        multiple: [
                            {
                                search: "SERVICE_WORKER_ENABLE",
                                replace:
                                    argv.enableServiceWorker || argv.mode !== "development"
                                        ? "true"
                                        : "false",
                            },
                            {
                                search: "WEBPACK_VERSION",
                                replace: packageJson.version,
                            },
                            {
                                search: "WEBPACK_VERSION_NAME",
                                replace: packageJson.versionName,
                            },
                            {
                                search: "GIT_COMMIT_HASH",
                                replace: gitRev,
                            },
                            {
                                search: "WEBPACK_DATE",
                                replace: now.toString(),
                            },
                        ],
                    },
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader",
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "[name].[ext]",
                                outputPath: "fonts/",
                            },
                        },
                    ],
                },
                {
                    test: /\.s?[ac]ss$/i,
                    use: [
                        // Creates `style` nodes from JS strings
                        MiniCssExtractPlugin.loader,
                        // Translates CSS into CommonJS
                        "css-loader",
                        // Compiles Sass to CSS
                        "sass-loader",
                    ],
                },
                {
                    test: /\.vue$/,
                    loader: "vue-loader",
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: "src/index.html",
                inject: false,
                WID_THC_URL: process.env.WID_THC_URL || "http://localhost:3000",
            }),
            new CopyPlugin({
                patterns: [
                    { from: "src/img", to: "img" },
                    { from: "src/cgu.html", to: "." },
                    { from: "src/legals.html", to: "." },
                ],
            }),
            new VueLoaderPlugin(),
            new MiniCssExtractPlugin({
                filename: "wid.css",
            }),
            new WorkboxPlugin.InjectManifest({
                swSrc: "./src/js/services/ServiceWorker.js",
                maximumFileSizeToCacheInBytes: 5 * 1024 * 1024,
            }),
        ],
    };
};
