<div align="center">

# WID

![Logo Wid](https://gitlab.nuiton.org/uploads/-/system/project/avatar/252/src_img_logo.png?width=40)

What I did ?  
_The addictive time tracking software._

</div>

## Welcome in WID-cbd project

WID-cbd is the frontend part of the WID project.  
The backend part is hosted in [WID-thc project](https://gitlab.nuiton.org/jruchaud/wid-thc).

## How to develop on wid-cbd locally

1. Install
   `npm install`

2. Run dev mode
   `npm run dev`

3. Build
   `npm run build`

4. Docker build
   `docker build -f deploy/dockerfile -t wid-cbd:latest --rm .`

5. Docker run interractive mode
   `docker run -i -p 7878:80 wid-cbd:latest`

6. Docker run deamon mode
   `docker run -d -p 7878:80 wid-cbd:latest`

## How to contribute to WID project

Please refer to the [dedicated wiki page](@/../../../wikis)
