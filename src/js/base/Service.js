import { EventEmitter } from "events";

export class Service extends EventEmitter {
    constructor(context) {
        super();
        this.context = context;
    }
}
