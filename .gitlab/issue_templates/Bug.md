<!--
  Before creating a bug issue, please make sure it has not already been
  reported. Use the search engine to check in existing issues.
  Please also read https://gitlab.nuiton.org/jruchaud/wid-cbd/-/wikis/WID to
  find out which labels are to be used when reporting a bug.
-->

### Summary

### Steps to reproduce

### What is the current *bug* behavior?

### What is the expected *correct* behavior?

### Extra usefull informations (error message, browser version, screenshot, ...)
